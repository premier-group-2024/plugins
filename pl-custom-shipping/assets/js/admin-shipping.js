/**
 * admin-shipping.js
 *
 * Add admin shipping backend functions.
 *
 * @version 1.0
 */

jQuery( document ).ready( function( $ ) {
		
	if ( jQuery( '#woocommerce_pl_wc_custom_warehouses' ).length ) {
		
		jQuery( '#woocommerce_pl_wc_custom_warehouses' ).selectize( {
		    maxItems: 10,
		    plugins: ['remove_button'],
		    valueField: 'id',
		    labelField: 'title',
		    searchField: 'title',
		    options: [],
		    create: true
		} );
	}
		
	if ( jQuery( '.pl_free_selectize' ).length ) {
				
		jQuery( '.pl_free_selectize' ).selectize( {
		    maxItems: 9999,
		    plugins: ['remove_button'],
		    valueField: 'id',
		    labelField: 'title',
		    searchField: 'title',
		    create: true
		} );
	}
	
	if ( jQuery( '.pl_list_categories' ).length ) {
				
		jQuery( '.pl_list_categories' ).selectize( {
		    maxItems: 9999,
		    plugins: ['remove_button'],
		    valueField: 'id',
		    labelField: 'title',
		    searchField: 'title',
		    options: pl_settings.categories,
		    create: false
		} );
	}
} );