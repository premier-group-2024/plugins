<?php
	
if ( !class_exists( 'PL_Admin' ) ) {

	class PL_Admin {
							
	    public function __construct() {
		    
		    // Enqueue settings scripts
		    add_action( 'admin_enqueue_scripts', array( $this, 'settings_scripts' ) );
		}
		
		public function settings_scripts() {
			
			wp_enqueue_style( 'pl-csmt-selectize', plugins_url( 'assets/css/selectize.css', PL_CSMT_PLUGIN_FILE ) );
		    wp_enqueue_script( 'pl-csmt-selectize', plugins_url( 'assets/js/selectize.js', PL_CSMT_PLUGIN_FILE ) );
		    wp_enqueue_script( 'pl-csmt-admin-shipping', plugins_url( 'assets/js/admin-shipping.js', PL_CSMT_PLUGIN_FILE ) );
		    
		    $categories_raw = get_terms( 'product_cat', array( 'hide_empty' => false ) );
		    $categories     = array();
		    
		    foreach ( $categories_raw as $category_raw ) {
			    $categories[] = array( 'id' => $category_raw->term_id, 'title' => $category_raw->name );
		    }
		    
		    $data           = array( 'categories' => $categories );
		    wp_localize_script( 'pl-csmt-admin-shipping', 'pl_settings', $data );
		}		
	}
}