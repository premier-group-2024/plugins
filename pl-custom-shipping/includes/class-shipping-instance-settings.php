<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// General settings.
$settings = array(
	'title' => array(
		'title'       => __( 'Method title', 'pl-custom-shipping' ),
		'type'        => 'text',
		'description' => __( 'This controls the title which the user sees during checkout.', 'pl-custom-shipping' ),
		'default'     => __( 'Custom shipping', 'pl-custom-shipping' ),
		'desc_tip'    => true,
		'css'         => 'width:100%',
	),
	'other_flat_rate' => array(
		'title'       => __( 'Non Warehouse Flat Rate', 'pl-custom-shipping' ),
		'type'        => 'text',
		'description' => __( 'Flat rate to apply to each item that is not from a warehouse category.', 'pl-custom-shipping' ),
		'desc_tip'    => true,
		'default'     => '0',
		'css'         => 'width:100%',
	),
);

if ( $this->weight_ranges ) {
	foreach ( $this->weight_ranges as $weight ) {		
		$settings["other_flat_weight_rate_$weight"] = array(
			'title'           => $weight . ' ' . __( 'non Warehouse Flat Rate', 'pl-custom-shipping' ),
			'label'           => $weight . ' ' . __( 'non Warehouse Flat Rate', 'pl-custom-shipping' ),
			'type'        => 'text',
			'placeholder' => '',
			'description' => __( 'Flat rate to apply to each item that is not from a warehouse category depending on weight.', 'pl-custom-shipping' ),
			'default'     => '0',
			'desc_tip'    => true,
			'css'         => 'width:100%',
		);
	}
}

if ( $this->warehouses ) {
	foreach ( $this->warehouses as $warehouse ) {
		
		$warehouse_slug = sanitize_title( $warehouse );
		$settings["pallet_cost_warehouse_$warehouse_slug"] = array(
			'title'           => $warehouse . ' ' . __( 'base pallet cost', 'pl-custom-shipping' ),
			'label'           => $warehouse . ' ' . __( 'base pallet cost', 'pl-custom-shipping' ),
			'type'        => 'text',
			'placeholder' => '',
			'description' => __( 'Base Price to charge up to base pallet limit.', 'pl-custom-shipping' ),
			'default'     => '0',
			'desc_tip'    => true,
			'css'         => 'width:100%',
		);
		
		$settings["pallet_cost_extra_warehouse_$warehouse_slug"] = array(
			'title'           => $warehouse . ' ' . __( 'extra pallet cost', 'pl-custom-shipping' ),
			'label'           => $warehouse . ' ' . __( 'extra pallet cost', 'pl-custom-shipping' ),
			'type'        => 'text',
			'placeholder' => '',
			'description' => __( 'Extra price to charge for each extra pallet based on warehouse.', 'pl-custom-shipping' ),
			'default'     => '0',
			'desc_tip'    => true,
			'css'         => 'width:100%',
		);
	}
}

return $settings;
