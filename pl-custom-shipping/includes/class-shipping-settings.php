<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

// General settings.
$settings = array(
	'warehouses' => array(
		'title'           => __( 'Warehouses List', 'pl-custom-shipping' ),
		'label'           => __( 'Warehouses List', 'pl-custom-shipping' ),
		'type'            => 'text',
		'default'         => '',
		'desc_tip'        => true,
		'description'     => __( 'List of available warehouses.', 'pl-custom-shipping' )
	),
	'mixed_pallet_weight' => array(
		'title'       => __( 'Mixed pallet max weight (Kg)', 'pl-custom-shipping' ),
		'type'        => 'text',
		'description' => __( 'Maximum weight for a mixed pallet.', 'pl-custom-shipping' ),
		'desc_tip'    => true,
		'default'     => 1200,
		'css'         => 'width:100%',
	),
);

if ( $this->warehouses ) {
	foreach ( $this->warehouses as $warehouse ) {
		
		$warehouse_slug = sanitize_title( $warehouse );
		$settings["warehouse_category_$warehouse_slug"] = array(
			'title'           => $warehouse . ' ' . __( 'categories list', 'pl-custom-shipping' ),
			'label'           => $warehouse . ' ' . __( 'categories list', 'pl-custom-shipping' ),
			'type'            => 'text',
			'default'         => '',
			'desc_tip'        => true,
			'class'			  => 'pl_list_categories',
			'description'     => __( 'Sets categories list associated with this warehouse.', 'pl-custom-shipping' )
		);
	}
}

$settings['fee_label'] = array(
	'title'           => __( 'Extra Fee Label', 'pl-custom-shipping' ),
	'type'            => 'text',
	'default'         => 'Extra Fee',
	'desc_tip'        => true,
	'description'     => __( 'Extra fee label for carts with products from the bellow categories.', 'pl-custom-shipping' )
);

$settings['fee_cats'] = array(
	'title'           => __( 'Extra Fee Categories', 'pl-custom-shipping' ),
	'type'            => 'text',
	'default'         => '',
	'desc_tip'        => true,
	'class'			  => 'pl_list_categories',
	'description'     => __( 'Extra fee categories for carts with products from this categories.', 'pl-custom-shipping' )
);


$settings['fee_value'] = array(
	'title'           => __( 'Extra Fee Value', 'pl-custom-shipping' ),
	'type'            => 'text',
	'default'         => '',
	'desc_tip'        => true,
	'class'			  => '',
	'description'     => __( 'Extra fee value for carts with products from above categories.', 'pl-custom-shipping' )
);

$settings['weight_ranges'] = array(
	'title'           => __( 'Non Warehouse Weight Ranges', 'pl-custom-shipping' ),
	'type'            => 'text',
	'default'         => '',
	'desc_tip'        => true,
	'class'			  => 'pl_free_selectize',
	'description'     => __( 'Defines weight ranges for non warehouse fees. Fees are defined on each shipping zone', 'pl-custom-shipping' )
);

$settings['pickup_mode'] = array(
	'title'           => __( 'Pickup Mode', 'pl-custom-shipping' ),
	'type'            => 'select',
	'options'		  => array( 
		'' 		 => __( 'Don\'t allow pickups', 'pl-custom-shipping' ),
		'single' => __( 'Allow single warehouse pickups', 'pl-custom-shipping' ),
		'multi'  => __( 'Allow multiple warehouse pickups', 'pl-custom-shipping' ),
	),
	'default'         => '',
	'desc_tip'        => true,
	'description'     => __( 'Defines pickup mode, to allow single warehouse pickup, multiple warehouse pickup or none.', 'pl-custom-shipping' )
);

$settings['pickup_title'] = array(
	'title'           => __( 'Title to show for pickup method', 'pl-custom-shipping' ),
	'type'            => 'text',
	'default'         => '',
	'desc_tip'        => true,
	'description'     => __( 'Defines title to show for pickup options on frontend.', 'pl-custom-shipping' )
);

if ( $this->warehouses ) {
	foreach ( $this->warehouses as $warehouse ) {
		
		$warehouse_slug = sanitize_title( $warehouse );
		$settings["warehouse_pickup_message_$warehouse_slug"] = array(
			'title'           => $warehouse . ' ' . __( 'pickup message', 'pl-custom-shipping' ),
			'label'           => $warehouse . ' ' . __( 'pickup message', 'pl-custom-shipping' ),
			'type'            => 'text',
			'default'         => '',
			'desc_tip'        => true,
			'class'			  => '',
			'description'     => __( 'Sets pickup message for each warehouse.', 'pl-custom-shipping' )
		);
	}
}

$settings['debug'] = array(
	'title'           => __( 'Debug Mode', 'pl-custom-shipping' ),
	'label'           => __( 'Enable debug mode', 'pl-custom-shipping' ),
	'type'            => 'checkbox',
	'default'         => 'no',
	'desc_tip'    => true,
	'description'     => __( 'Enable debug mode to show debugging information on the cart/checkout.', 'pl-custom-shipping' )
);

return $settings;
