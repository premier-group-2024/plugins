<?php
if ( !class_exists( 'PL_Shipping' ) ) {

	class PL_Shipping {
							
	    public function __construct() {
		    
		    // Add shipping method file 
			add_action( 'init', array( $this, 'add_shipping_file' ) );
			
			// Add shipping method class
			add_filter( 'woocommerce_shipping_methods', array( $this, 'add_shipping_class' ) );
			
			// Maybe add extra fee
			add_action( 'woocommerce_cart_calculate_fees', array( $this, 'maybe_add_extra_fee' ) );
			
			// Filter pickup shipping methods
			add_filter( 'woocommerce_package_rates', array( $this, 'hide_pickup_locations' ), 100 );
			
			// Show warehouse association
			add_filter( 'woocommerce_get_item_data', array( $this, 'maybe_show_warehouse_details_on_cart' ), 10, 3 );
			
			// Add warehouse association to product order
		    add_action( 'woocommerce_add_order_item_meta', array( $this, 'add_order_item_data' ), 10, 3 );
		    
		    // Change warehouse association key and value
		    add_filter( 'woocommerce_order_item_display_meta_key', array( $this, 'filter_order_item_labels' ), 20, 3 );
		    add_filter( 'woocommerce_order_item_display_meta_value', array( $this, 'filter_order_item_values' ), 20, 3 );
		}
		
		public function filter_order_item_values( $display_value, $meta, $item ) {
			
			if ( $item->get_type() === 'line_item' && $meta->key === 'pl_warehouse' ) {
		        $display_value = $this->add_warehouse_message( $display_value );
		    }
		    
		    return $display_value;
		}
		
		public function filter_order_item_labels( $display_key, $meta, $item ) {
			
			if ( $item->get_type() === 'line_item' && $meta->key === 'pl_warehouse' ) {
		        $display_key = __( "Warehouse", "pl-custom-shipping" );
		    }
		    
		    return $display_key;
		}
		
		public function add_order_item_data( $itemId, $values, $key ) {
		    
		    $selected_method = isset( $_REQUEST['shipping_method'][0] ) ? $_REQUEST['shipping_method'][0] : '';
		    if ( $selected_method == 'pl_wc_custom_pickup' ) {
			    
			    $product = isset( $values['data'] ) ? $values['data'] : array();
			    if ( $product ) {
				    $warehouse = $this->get_products_warehouse( $product );
				    if ( $warehouse ) {
					    wc_add_order_item_meta( $itemId, 'pl_warehouse', $warehouse );
				    }
			    }
		    }	    
	    }
		
		public function maybe_show_warehouse_details_on_cart( $data, $cartItem ) {
			
			$product = isset( $cartItem['data'] ) ? $cartItem['data'] : array();
			if ( $product ) {
				$warehouse = $this->get_products_warehouse( $product );
				if ( $warehouse ) {
					$data[] = array(
			            'name'  => __( 'Warehouse', 'pl-custom-shipping' ),
			            'value' => $warehouse
			        );
				}				
			}
			
			return $data;
		}
		
		public function add_warehouse_message( $warehouse ) {
			
			$s_warehouse = sanitize_title( $warehouse );
			$settings 	 = get_option( 'woocommerce_pl_wc_custom_settings' );
			if ( isset( $settings['warehouse_pickup_message_' . $s_warehouse ] ) ) {
				return $settings['warehouse_pickup_message_' . $s_warehouse ];
			}

			return $warehouse;
		}
		
		public function hide_pickup_locations( $methods ) {
			
			foreach ( $methods as $method_k => $method ) {
				if ( $method->get_method_id() == 'local_pickup' ) {
					unset( $methods[ $method_k ] );
				}
			}
						
			return $methods;
		}
				
		public function get_products_warehouse( $product_obj ) {
			
			$settings   	 = get_option( 'woocommerce_pl_wc_custom_settings' );
			$warehouses      = isset( $settings['warehouses'] ) ? explode( ',', $settings['warehouses'] ) : array();
			if ( $warehouses ) {
				
				$warehouse_categories = array();				
				foreach ( $warehouses as $warehouse ) {
					$cat_slug = "warehouse_category_" . sanitize_title( $warehouse );
					if ( isset( $settings[ $cat_slug ] ) ) {
						$warehouse_categories[ $warehouse ] = explode( ',', $settings[ $cat_slug ] ); 
					}
				}
				
				$product_id  = $product_obj->get_id();
				if ( $product_obj instanceof WC_Product_Variation ) {
					$parent_product = wc_get_product( $product_obj->get_parent_id() );
					$categories     = $parent_product->get_category_ids();
				} else {
					$categories = $product_obj->get_category_ids();
				}
				
				if ( $categories ) {
						
					$exists = false;
					foreach ( $warehouse_categories as $warehouse_cat_k => $warehouse_cats ) {
						$shared_cats = array_intersect( $warehouse_cats, $categories );
						if ( $shared_cats ) {
							return $warehouse_cat_k;
						}
					}
				}
				
				$pickup_warehouse  = get_post_meta( $product_obj->get_id(), '_pl_pickup_warehouse', TRUE );
				if ( $pickup_warehouse ) {
					$pickup_warehouse = ucwords( str_replace( '-', ' ', $pickup_warehouse ) );
					return $pickup_warehouse;
				}
			}
						
			return false;
		}
				
		public function maybe_add_extra_fee( $cart ) {	
					
			if ( is_admin() && ! defined( 'DOING_AJAX' ) )
				return;
				
			$shipping_methods = WC()->session->get( 'chosen_shipping_methods' );
			$shipping_method  = $shipping_methods ? $shipping_methods[0] : '';
			if ( $shipping_method == 'pl_wc_custom_pickup' ) {
				return;
			}
				
			$settings   = get_option( 'woocommerce_pl_wc_custom_settings' );
			$cart_items = $cart->cart_contents;
			$fee_cats   = $settings['fee_cats'] ? explode( ',', $settings['fee_cats'] ) : array();
			$fee_label  = $settings['fee_label'];
			$fee_value  = $settings['fee_value'];
			$fee_qtty   = 0;
			foreach ( $cart_items as $cart_item ) {
				
				$quantity         = $cart_item['quantity'];
				$product_obj      = $cart_item['data'];
				if ( $product_obj ) {
					if ( $product_obj instanceof WC_Product_Variation ) {
						$parent_product = wc_get_product( $product_obj->get_parent_id() );
						$categories = $parent_product->get_category_ids();
					} else {
						$categories = $product_obj->get_category_ids();
					}
					
					$merged_cats = array_intersect( $fee_cats, $categories );
					
					if ( $merged_cats ) {
						$fee_qtty += $quantity;
					}				
				}
			}
			
			if ( $fee_qtty && $fee_label && $fee_value ) {
				$fee_total = $fee_value * $fee_qtty;
				$result = $cart->add_fee( $fee_label, $fee_total, false, '' );
			}
		}
		
		public function add_shipping_file() {
			require_once 'class-wc-shipping-custom.php';
		}
		
		public function add_shipping_class( $methods ) {
			
			$methods['pl_wc_custom'] = 'PL_Shipping_Custom';
			return $methods;
		}
	}
}