<?php
	
if ( !class_exists( 'PL_Products' ) ) {

	class PL_Products {
							
	    public function __construct() {
		    
		    // Add Product Fields
			add_action( 'woocommerce_product_options_shipping_product_data', array( $this, 'add_product_fields' ) );
			
			// Save Product Fields
			add_action( 'woocommerce_process_product_meta', array( $this, 'save_product_fields' ), 99 );  
			
			// Maybe add warehouse to product attribute
			add_filter( 'woocommerce_display_product_attributes', array( $this, 'maybe_add_warehouse' ), 999, 2 );
		}
		
		public function maybe_add_warehouse( $product_attributes, $product ) {
			
			$warehouse = $product->get_meta( '_pl_pickup_warehouse' );
			
			if ( $warehouse ) {
				$product_attributes['pl_pickup_warehouse'] = array( 
					'label' => __( 'Pickup at / Shipped from', 'pl-custom-shipping' ),
					'value' => '<p>' . ucwords( str_replace( '-', ' ', $warehouse ) ) . '</p>'
				);
			}
			
			return $product_attributes;
		}
				
		public function get_products_warehouse( $product_id ) {
			
			$product_obj = wc_get_product( $product_id );
			if ( !$product_obj ) {
				return false;
			}
			
			$settings   	 = get_option( 'woocommerce_pl_wc_custom_settings' );
			$warehouses      = isset( $settings['warehouses'] ) ? explode( ',', $settings['warehouses'] ) : array();
			if ( $warehouses ) {
				
				$warehouse_categories = array();				
				foreach ( $warehouses as $warehouse ) {
					$cat_slug = "warehouse_category_" . sanitize_title( $warehouse );
					if ( isset( $settings[ $cat_slug ] ) ) {
						$warehouse_categories[ $warehouse ] = explode( ',', $settings[ $cat_slug ] ); 
					}
				}
				
				$product_id  = $product_obj->get_id();
				if ( $product_obj instanceof WC_Product_Variation ) {
					$parent_product = wc_get_product( $product_obj->get_parent_id() );
					$categories     = $parent_product->get_category_ids();
				} else {
					$categories = $product_obj->get_category_ids();
				}
				
				if ( $categories ) {
						
					$exists = false;
					foreach ( $warehouse_categories as $warehouse_cat_k => $warehouse_cats ) {
						$shared_cats = array_intersect( $warehouse_cats, $categories );
						if ( $shared_cats ) {
							return $warehouse_cat_k;
						}
					}
				}
			}
						
			return false;
		}
		
		public function get_pickup_warehouses() {
			$warehouses = array( '' => __( 'None', 'pl-custom-shipping' ) );
			
			$settings   	 = get_option( 'woocommerce_pl_wc_custom_settings' );
			$raw_warehouses  = isset( $settings['warehouses'] ) ? explode( ',', $settings['warehouses'] ) : array();
			if ( $raw_warehouses ) {
				foreach ( $raw_warehouses as $warehouse ) {
					$slug = sanitize_title( $warehouse );
					$warehouses[ $slug ] = $warehouse;
				}
			}
			
			return $warehouses;
		}
		
		public function add_product_fields() {
			
			global $post;
			$pallet_items     = get_post_meta( $post->ID, '_pl_pallet_items', TRUE ) ? get_post_meta( $post->ID, '_pl_pallet_items', TRUE ) : 1;
			$min_pallet_items = get_post_meta( $post->ID, '_pl_min_pallet_items', TRUE ) ? get_post_meta( $post->ID, '_pl_min_pallet_items', TRUE ) : 1;	
			$base_fee_pallets = get_post_meta( $post->ID, '_pl_base_fee_pallets', TRUE ) ? get_post_meta( $post->ID, '_pl_base_fee_pallets', TRUE ) : 1;			
			$options          = array();
			
			echo '<div class="options_group">';
								    
		    woocommerce_wp_text_input(
				array(
					'id'          => '_pl_pallet_items',
					'label'       => __( 'Units', 'pl-custom-shipping' ),
					'placeholder' => '',
					'description' => __( 'Number of items that fill a pallet.', 'pl-custom-shipping' ),
					'desc_tip'    => true,
					'type'        => 'number', 
					'custom_attributes' => array( 'step' => '1', 'min' => '1' ),
					'value'       => $pallet_items,
				)
			);
			
			woocommerce_wp_text_input(
				array(
					'id'          => '_pl_min_pallet_items',
					'label'       => __( 'Minimum items per pallet', 'pl-custom-shipping' ),
					'placeholder' => '',
					'description' => __( 'Number of minimum items per pallet.', 'pl-custom-shipping' ),
					'desc_tip'    => true,
					'type'        => 'number', 
					'custom_attributes' => array( 'step' => '1', 'min' => '1' ),
					'value'       => $min_pallet_items,
				)
			);	
			
			woocommerce_wp_text_input(
				array(
					'id'          => '_pl_base_fee_pallets',
					'label'       => __( 'Base Fee Pallets', 'pl-custom-shipping' ),
					'placeholder' => '',
					'description' => __( 'Number of pallets that pay the base fee. Extra pallets pay a premium.', 'pl-custom-shipping' ),
					'desc_tip'    => true,
					'type'        => 'number', 
					'custom_attributes' => array( 'step' => '1', 'min' => '1' ),
					'value'       => $base_fee_pallets,
				)
			);	 
			
			$product = wc_get_product( $post->ID );
			
			if ( !$this->get_products_warehouse( $post->ID ) ) {
				
				$pickup_warehouse  = get_post_meta( $post->ID, '_pl_pickup_warehouse', TRUE );
				$warehouse_options = $this->get_pickup_warehouses();
								
				woocommerce_wp_select( 
					array( 
						'id'      	    => '_pl_pickup_warehouse', 
						'label'   	    => __( 'Pickup Warehouse', 'pl-custom-shipping' ), 
						'wrapper_class' => 'select short',
						'desc_tip'      => 'true',
						'description'   => __( 'Select which warehouse this product can be picked up from (if any).', 'pl-custom-shipping' ),
						'options'	    => $warehouse_options,
						'value'			=> $pickup_warehouse
					)
				);
			}   
		    
			echo '</div>';
		}
		
		public function save_product_fields( $post_id ) {
			
			$pallet_items = isset( $_POST['_pl_pallet_items'] ) ? $_POST['_pl_pallet_items'] : null;
			if ( !is_null( $pallet_items ) ) {
				update_post_meta( $post_id, '_pl_pallet_items', esc_attr( $pallet_items ) );
			}
			
			$min_pallet_items = isset( $_POST['_pl_min_pallet_items'] ) ? $_POST['_pl_min_pallet_items'] : null;
			if ( !is_null( $min_pallet_items ) ) {
				update_post_meta( $post_id, '_pl_min_pallet_items', esc_attr( $min_pallet_items ) );
			}
			
			$base_fee_pallets = isset( $_POST['_pl_base_fee_pallets'] ) ? $_POST['_pl_base_fee_pallets'] : null;
			if ( !is_null( $base_fee_pallets ) ) {
				update_post_meta( $post_id, '_pl_base_fee_pallets', esc_attr( $base_fee_pallets ) );
			}
			
			$pickup_warehouse = isset( $_POST['_pl_pickup_warehouse'] ) ? $_POST['_pl_pickup_warehouse'] : null;
			if ( !is_null( $pickup_warehouse ) ) {
				update_post_meta( $post_id, '_pl_pickup_warehouse', esc_attr( $pickup_warehouse ) );
			}
			
			
		}
	}
}