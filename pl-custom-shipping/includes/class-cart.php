<?php
	
if ( !class_exists( 'PL_Cart' ) ) {

	class PL_Cart {
							
	    public function __construct() {
		    
		    // Validate pallet sizes
		    add_action( 'woocommerce_check_cart_items', array( $this, 'validate_pallet_sizes' ), 1 ); 
		}
		
		public function validate_pallet_sizes() {
			
			$cart_items = WC()->cart->cart_contents;
			foreach ( $cart_items as $cart_item ) {
				
				$product_id       = $cart_item['product_id'];
				$quantity         = $cart_item['quantity'];
				$product_obj      = $cart_item['data'];
				$pallet_items     = get_post_meta( $product_id, '_pl_pallet_items', TRUE );
				$min_pallet_items = get_post_meta( $product_id, '_pl_min_pallet_items', TRUE );
				if ( $pallet_items && $min_pallet_items ) {
					$min = $quantity % $pallet_items;
					
					if ( $min > 0 && $min < $min_pallet_items ) {
						
						$diff          = $min_pallet_items - $min;
						$min_value     = $quantity + $diff;
						$product_title = $product_obj->get_title();
						$error_message = $product_title . ' ' . __( 'has invalid quantity, please select a value above', 'pl-custom-shipping' ) . ' ' . $min_value;
						wc_add_notice( $error_message, 'error' );
					}
				}
			}
		}
	}
}