<?php
	
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! class_exists( 'PL_Shipping_Custom' ) ) {

	/**
	 * Custom Shipping Method Class.
	 */
	class PL_Shipping_Custom extends WC_Shipping_Method {

		public function __construct( $instance_id = 0 ) {
			$this->id                 = 'pl_wc_custom';
			$this->id_pickup          = 'pl_wc_custom_pickup';
			$this->instance_id        = absint( $instance_id );
			$this->method_title       = __( 'HIAB To Site Freight', 'pl-custom-shipping' );
			$this->method_description = __( 'HIAB To Site Freight Shipping Method.', 'pl-custom-shipping' );
			$this->supports           = array(
				'shipping-zones',
				'instance-settings',
				'instance-settings-modal',
				'settings',
			);
			
			$this->init();
			add_action( 'woocommerce_update_options_shipping_' . $this->id, array( $this, 'process_admin_options' ) );			
		}
		
		public function init() {
			
			$this->warehouses             = $this->get_option( 'warehouses' )    ? explode( ',', $this->get_option( 'warehouses' ) )    : array();
			$this->weight_ranges          = $this->get_option( 'weight_ranges' ) ? explode( ',', $this->get_option( 'weight_ranges' ) ) : array();
			$this->pickup_mode            = $this->get_option( 'pickup_mode' );
			$this->pickup_title           = $this->get_option( 'pickup_title' );
			$this->instance_form_fields   = include 'class-shipping-instance-settings.php';
			$this->title                  = $this->get_option( 'title' );
			$this->other_flat_rate        = $this->get_option( 'other_flat_rate' );
			$this->mixed_pallet_weight    = $this->get_option( 'mixed_pallet_weight' ) ? $this->get_option( 'mixed_pallet_weight' ) : 1200;
			$this->warehouses_cats        = array();
			$this->warehouses_costs       = array();
			$this->warehouses_extra_costs = array();
			$this->weight_flat_rates      = array();
			$this->fee_label              = $this->get_option( 'fee_label' );
			$this->fee_cats               = $this->get_option( 'fee_cats' );
			$this->fee_value              = $this->get_option( 'fee_value' );
						
			foreach ( $this->warehouses as $warehouse ) {
				
				$warehouse_slug = sanitize_title( $warehouse );
				if ( $new_cat = $this->get_option( "warehouse_category_$warehouse_slug" ) ) {
					$this->warehouses_cats[ $warehouse_slug ] = explode( ',', $new_cat );
				}
				
				if ( $new_cost = $this->get_option( "pallet_cost_warehouse_$warehouse_slug" ) ) {
					$this->warehouses_costs[ $warehouse_slug ] = $new_cost;
				}
				
				if ( $new_cost = $this->get_option( "pallet_cost_extra_warehouse_$warehouse_slug" ) ) {
					$this->warehouses_extra_costs[ $warehouse_slug ] = $new_cost;
				}
			}
			
			foreach ( $this->weight_ranges as $weight_range ) {
				
				if ( $this->get_option( "other_flat_weight_rate_$weight_range" ) >= 0 ) {
					$new_weight = $this->get_option( "other_flat_weight_rate_$weight_range" );
					$this->weight_flat_rates[ $weight_range ] = $new_weight;
				}
			}
									
			$this->debug                = $this->get_option( 'debug' );
			$this->form_fields          = include 'class-shipping-settings.php';			
		}
		
		public function is_available( $package ) {
			
			$available = parent::is_available( $package );
			return $available;
		}
		
		public function calculate_mixed_pallets( $warehouse_products ) {
			
			$total      = 0;
			$max_weight = $this->mixed_pallet_weight;
			
			foreach ( $warehouse_products as $warehouse_slug => $warehouse_data ) {
				
				$warehouse_cost       = isset( $this->warehouses_costs[ $warehouse_slug ] )       ? $this->warehouses_costs[ $warehouse_slug ]       : 0;
				$warehouse_extra_cost = isset( $this->warehouses_extra_costs[ $warehouse_slug ] ) ? $this->warehouses_extra_costs[ $warehouse_slug ] : 0;
				$warehouse_weight     = 0;
				$mixed_pallets        = 0;
				$mixed_filled         = false;
				$extra_pallets        = 0;
				$base_pallets         = 0;
				foreach ( $warehouse_data as $base_pallet_qtty => $products ) {
					
					usort( $products, array( $this, 'sort_by_weight' ) );
					foreach ( $products as $product_id => $product_data ) {
						
						$product_pallets    = isset( $product_data['pallets'] ) ? $product_data['pallets'] : 1;
						$product_weight     = isset( $product_data['weight'] )  ? $product_data['weight']  : 1;
												
						if ( !$mixed_filled ) {
							if ( $product_pallets >= $base_pallet_qtty ) {
								
								$product_grouped_pallets = $product_pallets - $base_pallet_qtty;
								$extra_pallets += $product_grouped_pallets;
								$base_pallets++;
							} else {
								if ( ( $product_weight + $warehouse_weight ) <= $max_weight ) {
									$warehouse_weight += $product_weight;
									$mixed_pallets    += $product_pallets;
									if ( $mixed_pallets >= $base_pallet_qtty ) {
										if ( $mixed_pallets > 0 ) {
											$extra_pallets += ( $mixed_pallets % $base_pallet_qtty );
										}
										$mixed_pallets = $base_pallet_qtty;
										$mixed_filled  = true;
									}
								} else {
									
									$extra_pallets += $product_pallets;
									$mixed_filled   = true;
								}
							}							
						} else {
							
							if ( $product_pallets >= $base_pallet_qtty ) {
								$product_grouped_pallets = $product_pallets - $base_pallet_qtty;
							}
							
							$base_pallets  += 1;
							$extra_pallets += $product_grouped_pallets;
						}						
					}
				}
				
				if ( $mixed_pallets ) {
					$total += $warehouse_cost + ( $warehouse_extra_cost * $extra_pallets );
				} else if ( $extra_pallets ) {
					$total += $warehouse_cost + ( $warehouse_extra_cost * ( $extra_pallets - 1 ) );
				}
				
				$total += $base_pallets * $warehouse_cost;
			}		
								
			return $total;
		}
		
		public function sort_by_weight( $a, $b ) {
		    return $a['weight'] - $b['weight'];
		}


		public function calculate_shipping( $package = array() ) {
			
			$cart_items      = WC()->cart->cart_contents;
			$cost            = 0;
			$skip            = false;
			$ignore_pickup   = false;
			$pickups_outside = array();
			$warehouse_prods = array();
								
									
			foreach ( $cart_items as $cart_item ) {
				
				$passed           = false;
				$product_id       = $cart_item['product_id'];
				$quantity         = $cart_item['quantity'];
				$product_obj      = $cart_item['data'];
				$pallet_items     = get_post_meta( $product_id, '_pl_pallet_items', TRUE );
				$min_pallet_items = get_post_meta( $product_id, '_pl_min_pallet_items', TRUE );
				$base_pallets     = get_post_meta( $product_id, '_pl_base_fee_pallets', TRUE );
				$is_warehouse     = false;
				
				if ( $pallet_items && $min_pallet_items ) {
					$min = $quantity % $pallet_items;
					if ( !$min || $min >= $min_pallet_items ) {
						$pallets         = ceil( $quantity / $pallet_items );
						$passed          = true;
						$categories      = $product_obj->get_category_ids();
						$weight          = $product_obj->get_weight();
						$prod_weight     = $weight ? $weight * $quantity : 0;
						$extra_pallets   = $pallets - $base_pallets;
						$warehouse_costs = 0;
						
						if ( $product_obj instanceof WC_Product_Variation ) {
							$parent_product = wc_get_product( $product_obj->get_parent_id() );
							$categories     = $parent_product->get_category_ids();
						}
												
						foreach ( $categories as $category_id ) {
							foreach ( $this->warehouses_cats as $warehouse_slug => $warehouse_cats ) {
								if ( in_array( $category_id, $warehouse_cats ) ) {
									$is_warehouse   = true;
									
									if ( $prod_weight ) {
										$warehouse_prods[ $warehouse_slug ][ $base_pallets ][ $product_id ] = array( 'pallets' => $pallets, 'weight' => $prod_weight );
									} else {
										$warehouse_cost = isset( $this->warehouses_costs[ $warehouse_slug ] ) ? $this->warehouses_costs[ $warehouse_slug ] : 0;
										if ( $warehouse_cost ) {
											$warehouse_costs += $warehouse_cost;
										}
										
										if ( $extra_pallets > 0 ) {
											$warehouse_extra_cost = isset( $this->warehouses_extra_costs[ $warehouse_slug ] ) ? $this->warehouses_extra_costs[ $warehouse_slug ] : 0;
											if ( $warehouse_extra_cost ) {
												$warehouse_costs += $warehouse_extra_cost * $extra_pallets;
											}
										}
									}
								}
							}
						}
																			
						$cost += $warehouse_costs;
					}
				}
				
				if ( !$is_warehouse ) {
					
					$has_pickup = get_post_meta( $product_id, '_pl_pickup_warehouse', TRUE );
					if ( $has_pickup ) {
						$pickups_outside[ $has_pickup ] = 1;
					} else {
						$ignore_pickup = true;
					}
					
					$other_flat_rate = $this->other_flat_rate;
					
					foreach ( $this->weight_flat_rates as $weight_value => $weight_price ) {
						if ( is_numeric( $weight_price ) ) {
							$weight_array = explode( '-', $weight_value );
							$weight_min   = isset( $weight_array[0] ) ? $weight_array[0] * 1 : false;
							$weight_max   = isset( $weight_array[1] ) ? $weight_array[1] * 1 : false;
							if ( $weight_min !== false && $weight_max !== false ) {
								if ( $weight >= $weight_min && $weight_max > $weight ) {
									$other_flat_rate = $weight_price;
									break;
								}
							}
						}
					}
							
					$cost  += $other_flat_rate * $quantity;
					$passed = true;
				}
				
				if ( !$passed ) {
					$skip = true;
				}
			}
			
			if ( $warehouse_prods ) {
				$cost += $this->calculate_mixed_pallets( $warehouse_prods );
			}
			
			if ( !$skip ) {
				$rate = array(
					'id'      => $this->get_rate_id(),
					'label'   => $this->title,
					'cost'    => $cost,
					'package' => $package,
				);
				
				$this->add_rate( $rate );
			}
			
			$extra_pickup_warehouses = array();
			foreach ( $pickups_outside as $pickup_outside_k => $pickup_outside_v ) {
				if ( !isset( $warehouse_prods[ $pickup_outside_k ] ) ) {
					$extra_pickup_warehouses[ $pickup_outside_k ] = 1;
				}
			}
			
			$warehouse_total_prods = count( $warehouse_prods ) + count( $extra_pickup_warehouses );
									
			//$warehouse_prods
			if ( $this->pickup_mode && !$ignore_pickup ) {
				
				if ( $warehouse_total_prods == 1 ) {
					
					$warehouse_pickup = array_key_first( $warehouse_prods );
					if ( $warehouse_pickup ) {
						$warehouse_pickup = ucwords( strtok( $warehouse_pickup, '-' ) );
					}
					
					if ( !$warehouse_pickup && $extra_pickup_warehouses ) {
						$warehouse_pickup = array_key_first( $extra_pickup_warehouses );
						$warehouse_pickup = ucwords( strtok( $warehouse_pickup, '-' ) );
					} 
					
					$pickup_title 	  = $this->pickup_title ? $this->pickup_title : __( 'Click & Collect', 'pl-custom-shipping' );
					$pickup_title    .= ' - ' . $warehouse_pickup; 
					$pickup_rate = array(
						'id'      => $this->id_pickup,
						'label'   => $pickup_title,
						'cost'    => 0,
						'package' => $package,
					);
										
					$this->add_rate( $pickup_rate );
				}
				
				if ( $warehouse_total_prods > 1 && $this->pickup_mode == 'multi' ) {
					
					$warehouses_pickup = array_keys( $warehouse_prods );
					$warehouses_titles = array();
					
					foreach ( $warehouses_pickup as $warehouse_pickup ) {
						$warehouses_titles[] = ucwords( strtok( $warehouse_pickup, '-' ) );
					}
					
					if ( $extra_pickup_warehouses ) {
						foreach ( $extra_pickup_warehouses as $pickup_outside_k => $pickup_outside_v ) {
							$warehouses_titles[] = ucwords( strtok( $pickup_outside_k, '-' ) );
						}
					}
										
					$pickup_title 	  = $this->pickup_title ? $this->pickup_title : __( 'Click & Collect', 'pl-custom-shipping' );
					$pickup_title    .= ' - ' . implode( ' & ', $warehouses_titles ); 
					
					$pickup_rate = array(
						'id'      => $this->id_pickup,
						'label'   => $pickup_title,
						'cost'    => 0,
						'package' => $package,
					);
					
					$this->add_rate( $pickup_rate );
				}
			}
		}

		public function get_package_item_qty( $package ) {
			$total_quantity = 0;
			foreach ( $package['contents'] as $item_id => $values ) {
				if ( $values['quantity'] > 0 && $values['data']->needs_shipping() ) {
					$total_quantity += $values['quantity'];
				}
			}
			return $total_quantity;
		}
	}
}