<?php
/*
Plugin Name: HIAB To Site Freight Shipping Method
Description: Creates HIAB To Site Freight shipping method.
Version: 1.4
Author: Marcel Schmitz
Text Domain: pl-custom-shipping
Author URI: https://app.codeable.io/tasks/new?preferredContractor=22877
License: GPLv2
*/

if ( !class_exists( 'pl_custom_shipping' ) ) {

	class pl_custom_shipping {
		
		private static $instance = null;
		private static $data     = array();		
		public static function get_instance() {
			
			if ( self::$instance == null ) {
		      self::$instance = new pl_custom_shipping();
		    }
		 
		    return self::$instance;
		}
					
	    public function __construct() {
		    
		    // Load needed files
		    add_action( 'plugins_loaded', array( $this, 'includes' ) );		    
		}
		
		// Show notice if WooCommerce is not active
	    public function woocommerce_error_activation_notice() {
		    
		    $notice = __( 'You need WooCommerce active in order to use HIAB To Site Freight Shipping Method.', 'pl-custom-shipping' );
	  		echo "<div class='error'><p><strong>$notice</strong></p></div>";
	    }
	    		
		public function includes() {
			
			if ( !class_exists( 'WooCommerce' ) ) {
				add_action( 'admin_notices', array( $this, 'woocommerce_error_activation_notice' ) );
		    } else {
			    
			    if ( is_admin() ) {
				    include_once PL_CSMT_DIR_PATH . 'includes/admin/class-admin.php';
				    new PL_Admin();
			    }
			    
			    include_once PL_CSMT_DIR_PATH . 'includes/class-shipping.php';
			    include_once PL_CSMT_DIR_PATH . 'includes/class-products.php';
			    include_once PL_CSMT_DIR_PATH . 'includes/class-cart.php';
			   
			    new PL_Cart();
			    new PL_Shipping();
			    new PL_Products();
		    }
		}		
	}
}

if ( class_exists( 'pl_custom_shipping' ) ) {
	
	if ( ! defined( 'ABSPATH' ) ) {
	    exit; // Exit if accessed directly
	}
		
	define( 'PL_CSMT_DIR_PATH', plugin_dir_path( __FILE__ ) );
	define( 'PL_CSMT_DIR_URL', plugin_dir_url( __FILE__ ) );
	define( 'PL_CSMT_PLUGIN_BASENAME', plugin_basename( __FILE__ ) );
	define( 'PL_CSMT_PLUGIN_FILE', __FILE__ );
	define( 'PL_CSMT_PLUGIN_VERSION', '1.4' );
		
	pl_custom_shipping::get_instance();
}
